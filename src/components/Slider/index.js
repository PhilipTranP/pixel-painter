import React from 'react'
import './Slider.scss'

export default function Slider(props){
  return <input className="pixel-slider" type="range" min="0" max="100" onChange={e => props.handleChange(e.target.value)} />
}