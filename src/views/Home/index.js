import React from 'react'
import Slider from "../../components/Slider"
import './Home.scss'
import ImgSource from '../../assets/img/image1.png'


class Home extends React.Component {
  constructor(props) {
    super(props)
    this.imgRef = React.createRef()
    this.canvasRef = React.createRef()
    this.currentImg = null
    this.ctx = null
    this.state = {
      redInput: 0,
      greenInput: 0,
      blueInput: 0,
      brightnessInput: 0
    }
  }
  componentDidMount = () => {
    const canvas = this.canvasRef.current
    const img = this.imgRef.current
    const ctx = canvas.getContext("2d")
    this.ctx = ctx

    img.onload = () => {
      ctx.drawImage(img, 0, 0)
    }
    this.currentImg = img
  }

  handleBrightness = (brightness) => {
    const canvas = this.canvasRef.current

    const imageData = this.ctx.getImageData(0, 0, canvas.width, canvas.height);
    const data = imageData.data

    for (var i = 0; i < data.length; i += 4) {
      data[i] += 255 * (brightness / 100);
      data[i + 1] += 255 * (brightness / 100);
      data[i + 2] += 255 * (brightness / 100);

    }

    this.ctx.putImageData(imageData, 0, 0);

    return imageData
  }

  handleBrightnessSlider = (percentage) => {
    this.ctx.drawImage(this.currentImg, 0, 0);
    this.setState({ brightnessInput: percentage })
    const imageData = this.handleBrightness(percentage)
    this.handleRed(imageData, this.state.redInput)
  }

  handleRed = (imageData, redInput) => {
    const data = imageData.data
    for (var i = 0; i < data.length; i += 4) {
      data[i] += 255 * (redInput / 100)
    }
    this.ctx.putImageData(imageData, 0, 0)
  }


  handleRedSlider = (percentage) => {
    this.ctx.drawImage(this.currentImg, 0, 0)
    const imageData = this.handleBrightness(this.state.brightnessInput)
    this.handleRed(imageData, percentage)
    this.setState({ redInput: percentage })
  }


  handleGreen = (imageData, greenInput) => {
    const data =imageData.data
    for (var i = 0; i < data.length; i += 4) {
      data[i+1] += 255 * (greenInput / 100)
    }
    this.ctx.putImageData(imageData, 0, 0)
  }


  handleGreenSlider = (percentage) => {
    this.ctx.drawImage(this.currentImg, 0, 0)
    const imageData = this.handleBrightness(this.state.brightnessInput)
    this.handleGreen(imageData, percentage)
    this.setState({greenInput: percentage})
  }

  handleBlue = (imageData, blueInput) => {
    const data =imageData.data
    for (var i = 0; i < data.length; i += 4) {
      data[i+2] += 255 * (blueInput / 100)
    }
    this.ctx.putImageData(imageData, 0, 0)
  }

  handleBlueSlider = (percentage) => {
    this.ctx.drawImage(this.currentImg, 0, 0)
    const imageData = this.handleBrightness(this.state.brightnessInput)
    this.handleBlue(imageData, percentage)
    this.setState({blueInput: percentage})
  }


  render() {
    return (
      <div className="home-container">

        <h1>Hello Pixel Painter</h1>

        <canvas ref={this.canvasRef} width={740} height={425} />
        <img ref={this.imgRef} src={ImgSource} alt="Source" className="image-source" />


        <div className="slider-container">

          <span style={{backgroundColor: "black", background: "linear-gradient(.25turn,#000000, 10%, #ffffff)"}}><Slider handleChange={(percentage) => this.handleBrightnessSlider(percentage)} /></span>

          <span className="red-slider"><Slider handleChange={(percentage) => this.handleRedSlider(percentage)} /></span>

          <span className="green-slider"><Slider handleChange={(percentage) => this.handleGreenSlider(percentage)} /></span>

          <span className="slider blue-slider"><Slider handleChange={(percentage) => this.handleBlueSlider(percentage)} /></span>

        </div>

      </div>
    )
  }
}

export default Home


